@extends('layouts.app')

@section('content')

    <div class="main-container">
	
        <!-- Page Banner -->
        <div class="container-fluid no-left-padding no-right-padding page-banner">
            <!-- Container -->
            <div class="container">
                <h3>{{ $content->content['en']['name'] }}</h3>
                <nav class="breadcrumb">
                    <a class="breadcrumb-item" href="/">Главная</a>
                    <span class="breadcrumb-item active">{{ $content->content['en']['name'] }}</span>
                </nav>
            </div><!-- Container -->
        </div><!-- Page Banner /- -->

        <main class="site-main">
            
            <div class="container-fluid no-left-padding no-right-padding page-content about-me-section">
                <!-- Container -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="aboutme-content">
                                {!! $content->content['en']['body'] !!}
                            </div>
                        </div>
                    </div>
                </div><!-- Container /- -->
            </div>
            
        </main>
    
    </div>

@endsection