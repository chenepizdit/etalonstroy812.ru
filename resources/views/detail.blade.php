@extends('layouts.app')

@section('content')

    <!-- Page Banner -->
    <div class="container-fluid no-left-padding no-right-padding page-banner">
        <!-- Container -->
        <div class="container">
            <h3>{{ $content->content['en']['name'] }}</h3>
            <nav class="breadcrumb">
                <a class="breadcrumb-item" href="/">Главная</a>
                <span class="breadcrumb-item active">{{ $content->content['en']['name'] }}</span>
            </nav>
        </div><!-- Container -->
    </div><!-- Page Banner /- -->

    <main class="site-main">
        
        <!-- Page Content -->
        <div class="container-fluid no-left-padding no-right-padding page-content product-detail">
            <!-- Container -->
            <div class="container">
                <div class="type-product">
                    <div class="product-gallery">
                        <figure><img src="{{ $content->content['en']['picture'] }}" alt="Detail" /></figure>
                    </div>
                    <div class="entry-summary">
                        <h1 class="product_title">{{ $content->content['en']['name'] }}</h1>
                        <div class="woocommerce-product-details__short-description">
                            {!! $content->content['en']['body'] !!}
                        </div>
                    </div>
                    
                </div>
            </div><!-- Container /- -->
        </div><!-- Page Content /- -->

    </main>

@endsection