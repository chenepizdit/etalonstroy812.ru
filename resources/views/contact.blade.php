@extends('layouts.app')

@section('content')

	<div class="main-container">
		<!-- Page Banner -->
		<div class="container-fluid no-left-padding no-right-padding page-banner">
			<!-- Container -->
			<div class="container">
				<h3>{{ $content->content['en']['name'] }}</h3>
				<nav class="breadcrumb">
					<a class="breadcrumb-item" href="/">Главная</a>
					<span class="breadcrumb-item active">{{ $content->content['en']['name'] }}</span>
				</nav>
			</div><!-- Container -->
		</div>
	
		<main class="site-main">	
			<div class="container-fluid no-left-padding no-right-padding contact-section">
				<div class="container">
                    <div class="map-section">
						<div class="map-canvas" id="contact-map-canvas" data-lat="{{ $content->content['en']['place']['lat'] }}" data-lng="{{ $content->content['en']['place']['lng'] }}" data-zoom="10"></div>
					</div>
					<div class="contact-form">
						<h2>Обратная связь</h2>
						<form class="row" action="#" id="form-message">
							<div class="col-md-4 form-group">
								<input type="text" class="form-control" placeholder="Имя" name="contact-name" id="input_name" required>
							</div>
							<div class="col-md-4 form-group">
								<input type="email" class="form-control" placeholder="E-mail" name="contact-email"  id="input_email" required>
							</div>
							<div class="col-md-4 form-group">
								<input type="text" class="form-control" placeholder="Веб-сайт" name="contact-website"  id="input_website">
							</div>
							<div class="col-md-8 form-group">
								<textarea class="form-control" placeholder="Комментарий" name="textarea-message" rows="5" id="textarea_message" required></textarea>
							</div>
							<div class="col-md-4 form-group align-self-end">
								<button id="btn_submit" name="submit" class="submit">отправить<i class="fa fa-long-arrow-right" aria-hidden="true"></i></button>
							</div>
							<div id="alert-msg" class="alert-msg"></div>
						</form>
					</div>
				</div>		
			</div>
		</main>
		
	</div>

@endsection