<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">
    <meta name="author" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	
	<title>{{ $setting->content['en']['title'] }}</title>

	<link rel="icon" type="image/x-icon" href="/images//favicon.ico" />
	<link href="https://fonts.googleapis.com/css?family=Lato:100,100i,300,300i,400,400i,700,700i,900,900i%7cRaleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cOpen+Sans:300,300i,400,400i,600,600i,700,700i,800,800i%7cPoppins:300,400,500,600,700%7cKhand:300,400,500,600,700%7cMontserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i%7cPT+Sans:400,400i,700,700i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/css/settings.css">
    <link href="/css/lib.css" rel="stylesheet">
	<link rel="stylesheet" href="/css/rtl.css">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
</head>

<body data-offset="200" data-spy="scroll" data-target=".ownavigation">
	<!-- Loader -->
	<div id="site-loader" class="load-complete">
		<div class="loader">
			<div class="line-scale"><div></div><div></div><div></div><div></div><div></div></div>
		</div>
	</div><!-- Loader /- -->
		
	<!-- Header Section -->
	<header class="container-fluid header_s header-fix">
		<nav class="navbar ownavigation navbar-expand-lg">
			<a class="cst-link" href="/"><p class="image-logo" ><img src="/images/logo.png" alt="Logo"></p></a>
			<a class="cst-link" href="/"><p class="cst-logo">{{ $setting->content['en']['name'] }}</p></a>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
				<i class="fa fa-bars"></i>
			</button>
			<div class="collapse navbar-collapse" id="navbar">
				<ul class="navbar-nav">
                @foreach ($menu as $item)
					@if(empty($item->children()->first()))
					<li class="{{ (strripos($item->slug, Request::path())) ? 'active' : ''}}">
                        <a class="nav-link" title="{{ $item->label }}" 
							href="{{ $item->slug }}" target="{{ $item->target }}">{{ $item->title }}</a>
					</li>
					@else
					<li class="dropdown {{ (strripos($item->slug, Request::path())) ? 'active' : ''}}">
						<i class="ddl-switch fa fa-angle-down"></i>
						<a class="nav-link dropdown-toggle" title="{{ $item->label }}" 
							href="{{ $item->slug }}" target="{{ $item->target }}">{{ $item->title }}</a>
						<ul class="dropdown-menu">
							@foreach ($item->children()->get() as $child)
							<li>
								<a class="dropdown-item" href="{{ $child->slug }}" 
									title="{{ $child->label }}" target="{{ $child->target }}">{{ $child->title }}</a>
							</li>
							@endforeach
						</ul>
					</li>
					@endif
                @endforeach
				</ul>
			</div>
		</nav>
	</header><!-- Header Section /- -->

	<div class="main-container">

        @yield('content')
		
	</div>
	
	<!-- Footer Section -->
	<div class="container-fluid no-left-padding no-right-padding footer-main">		
		<!-- Footer Widget -->
		<div class="container-fluid footer-widget">
			<!-- Row -->
			<div class="row">
				<div class="col-xl-4 col-lg-4 col-sm-6 col-12">
					<!-- Widget : About -->
					<aside class="widget widget_about">
						<a href="#">{{ $setting->content['en']['name'] }}</a>						
						{!! $footer->content['en']['description'] !!}
					</aside><!-- Widget : About /- -->
				</div>
				<!-- <div class="col-xl-3 col-lg-4 col-sm-6 col-12">
					<aside id="recent-posts" class="widget widget_recent_entries">
						<h3 class="widget-title">Последние новости</h3>
						<ul>
							<li>
								<a href="#">Жилая застройка в границах улиц Московское шоссе, пер.Тупой, пр.К.Маркса</a>
							</li>
							<li>
								<a href="#">Спешим вам сообщить, что с 01.12.2018г. планируется плановое повышение цен</a>
							</li>
							<li>
								<a href="#">Спешим вам сообщить, что с 03.09.2018г. старт продаж квартир в ЖК "Центральный"</a>
							</li>
						</ul>
					</aside>
				</div> -->
				<div class="col-xl-4 col-lg-4 col-sm-6 col-12">
					<!-- Widget : Instagram -->
					<aside class="widget widget_recent_entries">
						<h3 class="widget-title">Быстрые ссылки</h3>
						<ul>
                            @foreach ($menu as $item)
                                <li>
                                    <a title="{{ $item->label }}" href="{{ $item->slug }}">{{ $item->title }}</a>
                                </li>
                            @endforeach
						</ul>
					</aside><!-- Widget : Instagram /- -->
				</div>
				<div class="col-xl-4 col-lg-4 col-sm-6 col-12">
					<!-- Widget : Contact Info -->
					<aside class="widget widget_contact_info">
						<h3 class="widget-title">Контакты</h3>
						<div class="contact-info">
							<p><i class="fa fa-phone"></i> <a href="tel:{{ $footer->content['en']['phone'] }}">{{ $footer->content['en']['phone'] }}</a></p>
							<p><i class="fa fa-envelope"></i> <a href="mailto:{{ $footer->content['en']['mail'] }}">{{ $footer->content['en']['mail'] }}</a></p>
							<p><i class="fa fa-map-marker"></i> {{ $footer->content['en']['city'] }}</p>
						</div>
					</aside><!-- Widget : Contact Info /- -->
				</div>
			</div><!-- Row /- -->
		</div><!-- Footer Widget /- -->
		<!-- Copyright Block -->
		<div class="container-fluid copyright-block">
			<div class="row">
				<div class="col-lg-5 col-12 copyright">
					<p>{!! $footer->content['en']['reserved'] !!}</p>
				</div>
				<div class="col-lg-2 col-12 order-lg-1 scroll-navigation">
					<a id="back-to-top" href="#"><i class="fa fa-angle-up"></i></a>
				</div>
			</div>
		</div><!-- Copyright Block -->
	</div><!-- Footer Section /- -->
	
	<!-- JQuery v1.12.4 -->
	<script src="/js/jquery-1.12.4.min.js"></script>

	<!-- Library - Js -->
	<script src="/js/popper.min.js"></script>
	<script src="/js/lib.js"></script>
	
    <!-- Library - Google Map API -->
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD0HWCKb5OtJXxQVNC_3V_SQ6dalSW5Qto" type="text/javascript"></script>

	<!-- Library - Theme JS -->
	<script src="/js/functions.js"></script>
	
</body>
</html>