@extends('layouts.app')

@section('content')

    <main class="site-main">
        
        <!-- Welcome Text -->
        <div class="container-fluid no-left-padding no-right-padding welcome-text">
            <!-- Container -->
            <div class="container">
                <h2>{{ $main->content['en']['name'] }}</h2>
            </div><!-- Container /- -->
        </div><!-- Welcome Text -->
        
        <!-- Portfolio Section -->
        <div class="container-fluid portfolio-section portfolio-4-col">				
            <!-- Portfolio List -->
            <div class="row portfolio-list">						
                @foreach ($images as $image)
                    <div class="col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12 portfolio-box">						
                        <div class="portfolio-detail">
                            <i><img src="{{$image->content['en']['picture']}}" alt="Gallery" /></i>
                            <div class="portfolio-content">
                                @if ( $main->content['en']['checkGoDetail'] == 1)
                                    <a href="/detail/{{$image->id}}">Подробнее</a>
                                @endif
                                <a href="{{$image->content['en']['picture']}}" class="zoom" title="{{$image->content['en']['name']}}"><i class="fa fa-search"></i></a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div><!-- Row -->
        </div><!-- Portfolio Section /- -->
        
        <!-- Quote Section -->
        <div class="container-fluid no-left-padding no-right-padding quote-section">
            <!-- Container -->
            <div class="container">
                {!! $main->content['en']['body'] !!}
            </div><!-- Container /- -->
        </div><!-- Quote Section /- -->
        
    </main>
@endsection