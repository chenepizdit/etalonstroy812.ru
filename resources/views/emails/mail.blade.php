<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
	<table>
        <tr>
            <td>Имя: </td><td>{{$name}}</td>
        </tr>
        <tr>
            <td>E-mail: </td><td>{{$email}}</td>
        </tr>
        @isset($website)
        <tr>
            <td>Веб-сайт: </td><td>{{$website}}</td>
        </tr>
        @endisset
        <tr>
            <td>Комментарий: </td><td>{{$comment}}</td>
        </tr>
    </table>
</body>
</html>