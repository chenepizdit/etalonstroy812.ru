<?php

declare(strict_types=1);

namespace App\Orchid\Entities;

use Orchid\Screen\Field;
use Orchid\Press\Entities\Single;
use Orchid\Screen\Fields\MapField;
use Orchid\Screen\Fields\UTMField;
use Orchid\Screen\Fields\CodeField;
use Orchid\Screen\Fields\TagsField;
use Orchid\Screen\Fields\InputField;
use Orchid\Screen\Fields\QuillField;
use Orchid\Screen\Fields\SelectField;
use Orchid\Screen\Fields\PictureField;
use Orchid\Screen\Fields\TinyMCEField;
use Orchid\Screen\Fields\CheckBoxField;
use Orchid\Screen\Fields\TextAreaField;
use Orchid\Screen\Fields\DateTimerField;
use Orchid\Screen\Fields\SimpleMDEField;

class Footer extends Single
{
    /**
     * @var string
     */
    public $name = 'Подвал';

    /**
     * @var string
     */
    public $description = 'Редактирование подвала';

    /**
     * @var string
     */
    public $slug = 'footer';

    /**
     * Slug url /news/{name}.
     *
     * @var string
     */
    public $slugFields = 'name';

    /**
     * Menu group name.
     *
     * @var null
     */
    public $groupname = null;

    /**
     * Rules Validation.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => 'sometimes|integer|unique:posts',
            'content.*.phone' => 'required|string',
            'content.*.mail' => 'required|string',
            'content.*.city' => 'required|string',
            'content.*.description' => 'required|string',
            'content.*.reserved' => 'required|string',
        ];
    }

    /**
     * @return array
     * @throws \Throwable|\Orchid\Screen\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [

            InputField::make('phone')
                ->type('text')
                ->max(255)
                ->required()
                ->title('Телефон')
                ->help('Номер телефона организации'),

            InputField::make('mail')
                ->type('text')
                ->max(255)
                ->required()
                ->title('E-mail')
                ->help('Электронный адрес'),

            InputField::make('city')
                ->type('text')
                ->max(255)
                ->required()
                ->title('Город')
                ->help('Расположение организации'),

            TinyMCEField::make('description')
                ->required()
                ->title('Описание организации')
                ->theme('modern'),

            TextAreaField::make('reserved')
                ->max(255)
                ->rows(5)
                ->required()
                ->title('Копирайт')
                ->help('Все права защищены'),

        ];
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function options(): array
    {
        return [];
    }
}
