<?php

declare(strict_types=1);

namespace App\Orchid\Entities;

use Orchid\Screen\Field;
use Orchid\Press\Entities\Single;
use Orchid\Screen\Fields\MapField;
use Orchid\Screen\Fields\UTMField;
use Orchid\Screen\Fields\CodeField;
use Orchid\Screen\Fields\TagsField;
use Orchid\Screen\Fields\InputField;
use Orchid\Screen\Fields\QuillField;
use Orchid\Screen\Fields\UploadField;
use Orchid\Screen\Fields\SelectField;
use Orchid\Screen\Fields\PictureField;
use Orchid\Screen\Fields\TinyMCEField;
use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\Fields\CheckBoxField;
use Orchid\Screen\Fields\TextAreaField;
use Orchid\Screen\Fields\DateTimerField;
use Orchid\Screen\Fields\SimpleMDEField;

class Main extends Single
{
    /**
     * @var string
     */
    public $name = 'Главная';

    /**
     * @var string
     */
    public $description = 'Редактирование главной страницы';

    /**
     * @var string
     */
    public $slug = 'main';

    /**
     * Slug url /news/{name}.
     *
     * @var string
     */
    public $slugFields = 'name';

    /**
     * Menu group name.
     *
     * @var null
     */
    public $groupname = null;

    /**
     * Rules Validation.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'id' => 'sometimes|integer|unique:posts',
            'content.*.name' => 'required|string',
            // 'content.*.body' => 'required|string',
        ];
    }

    /**
     * @return array
     * @throws \Throwable|\Orchid\Screen\Exceptions\TypeException
     */
    public function fields(): array
    {
        return [

            Field::group([

                InputField::make('name')
                    ->type('text')
                    ->max(255)
                    ->required()
                    ->title('Name Articles')
                    ->help('Article title'),

                // InputField::make('title')
                //     ->type('text')
                //     ->max(255)
                //     ->required()
                //     ->title('Article Title')
                //     ->help('SEO title'),

            ]),

            CheckBoxField::make('checkGoDetail')
                    ->sendTrueOrFalse()
                    ->title('Показ подробной информации')
                    ->placeholder('Включение подробной информации')
                    ->help('Возможнсть перехода на подробную информацию об проекте'),

            // Field::group([

            //     DateTimerField::make('open')
            //         ->title('Opening date')
            //         ->help('The opening event will take place'),

            //     InputField::make('phone')
            //         ->type('text')
            //         ->mask('(999) 999-9999')
            //         ->title('Phone')
            //         ->help('Number Phone'),

            //     CheckBoxField::make('free')
            //         ->sendTrueOrFalse()
            //         ->title('Free')
            //         ->placeholder('Event for free')
            //         ->help('Event for free'),
            // ]),

            // TextAreaField::make('description')
            //     ->max(255)
            //     ->rows(5)
            //     ->required()
            //     ->title('Short description'),

            TinyMCEField::make('body')
                ->required()
                ->title('Name Articles')
                ->help('Article title')
                ->theme('modern'),

            // MapField::make('place')
            //     ->required()
            //     ->title('Object on the map')
            //     ->help('Enter the coordinates, or use the search'),

            // PictureField::make('picture')
            //     ->name('picture')
            //     ->width(500)
            //     ->height(300),

            // UTMField::make('link')
            //     ->title('UTM link')
            //     ->help('Generated link'),

            // SelectField::make('robot.')
            //     ->options([
            //         'index' => 'Index',
            //         'noindex' => 'No index',
            //     ])
            //     ->multiple()
            //     ->title('Indexing')
            //     ->help('Allow search bots to index'),

            // TagsField::make('keywords')
            //     ->title('Keywords')
            //     ->help('SEO keywords'),

            // SimpleMDEField::make('body')
            //     ->title('Name Articles')
            //     ->help('Article title'),

            // QuillField::make('body1')
            //     ->title('Name Articles')
            //     ->help('Article title'),

            // CodeField::make('code')
            //     ->title('Name Articles')
            //     ->help('Article title'),
        ];
    }

    /**
     * @return array
     * @throws \Orchid\Screen\Exceptions\TypeException
     * @throws \Throwable
     */
    public function main(): array
    {
        return array_merge(parent::main(), [
            // UploadField::make('attachment')
            //     ->title('Upload DropBox'),
        ]);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(Model $model) : Model
    {
        return $model->load(['attachment', 'tags', 'taxonomies'])
            ->setAttribute('category', $model->taxonomies->map(function ($item) {
                return $item->id;
            })->toArray());
    }

    /**
     * @param \Illuminate\Database\Eloquent\Model $model
     */
    public function save(Model $model)
    {
        $model->save();

        $model->taxonomies()->sync(array_flatten(request(['category'])));
        $model->setTags(request('tags', []));
        $model->attachment()->syncWithoutDetaching(request('attachment', []));
    }

    /**
     * @return array
     * @throws \Throwable
     */
    public function options(): array
    {
        return [];
    }
}
