<?php

namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use Orchid\Press\Models\Menu;
use Orchid\Press\Models\Post;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use App\Http\Requests\MailRequest;
use Illuminate\Support\Facades\Storage;

class AppController extends Controller
{
    
    private $footer;
    private $menu;
    private $setting;

    public function __construct() {

        $this->footer = Post::where('slug', 'footer')->first();

        $this->setting = Post::where('slug', 'setting')->first();

        $this->menu = Menu::where('lang', app()->getLocale())
            ->where('parent',0)
            ->where('type', 'header')
            ->with('children')
            ->orderBy('sort')
            ->get();
    }
    
    public function index() {

        $main = Post::where('slug', 'main')->first();

        $images = Post::where('type', 'project')->get();

        return view('main', [
            'menu' => $this->menu,
            'images' => $images,
            'main' => $main,
            'footer' => $this->footer,
            'setting' => $this->setting,
        ]);
    }

    public function aboutme() {

        $content = Post::where('slug', 'aboutme')->first();

        return view('aboutme', [
            'menu' => $this->menu,
            'footer' => $this->footer,
            'content' => $content,
            'setting' => $this->setting,
        ]);
    }

    public function contact() {

        $content = Post::where('slug', 'contact')->first();
        
        return view('contact', [
            'menu' => $this->menu,
            'footer' => $this->footer,
            'content' => $content,
            'setting' => $this->setting,
        ]);

    }

    public function send(MailRequest $request) {
        $this->sendMail(
            $request['contact-name'], 
            $request['contact-email'],
            $request['contact-website'],
            $request['textarea-message']
        );
    }

    public function sendMail($name, $email, $website, $comment) {

        $toName = env('MAIL_NAME');
        $toEmail = env('MAIL_USERNAME');
        $fromEmail = env('MAIL_USERNAME');

        $data = [
            'name' => $name,
            'email' => $email,
            'website' => $website,
            'comment' => $comment
        ]; 
        
        Mail::send('emails.mail', $data, function($message) use ($toName, $toEmail, $fromEmail) {
            $message->to($toEmail, $toName)
                    ->subject('Эталон.Строй - обратная связь');
            $message->from($fromEmail, 'Обратная связь');
        });

    }

    public function detail($id) {
        
        $main = Post::where('slug', 'main')->first();
        if ($main->content['en']['checkGoDetail'] == 0) {
            abort(404);
        }

        $content = Post::where('type', 'project')->where('id', $id)->first();
        
        if(empty($content)) {
            abort(404);
        }

        return view('detail', [
            'menu' => $this->menu,
            'footer' => $this->footer,
            'content' => $content,
            'setting' => $this->setting,
        ]);
    }

}